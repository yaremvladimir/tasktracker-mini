package com.tasktracker.mini.controller;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.tasktracker.mini.TaskTrackerServerStarter;
import com.tasktracker.mini.entity.Attachment;
import com.tasktracker.mini.repository.AttachmentsRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TaskTrackerServerStarter.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class TasksAttachmentsControllerIntegrationTest {
    private final String FILE_NAME = "hello.txt";
    private final byte[] FILE_CONTENT = "Hello, World!".getBytes();
    private final MockMultipartFile FILE =
            new MockMultipartFile("file", FILE_NAME, MediaType.TEXT_PLAIN_VALUE, FILE_CONTENT);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AttachmentsRepository attachmentsRepository;

    @Test
    @DisplayName("Verify successful uploading of attachment")
    public void testSuccessfulUploadOfAttachment() throws Exception {
        MvcResult uploadResult = uploadAttachment();

        // verify status
        assertEquals(HttpStatus.OK.value(), uploadResult.getResponse().getStatus());

        Optional<Attachment> attachment = attachmentsRepository.findById(1L);
        // verify attachment exists in DB
        assertTrue(attachment.isPresent());

        // verify that it is our file
        assertEquals(FILE_NAME, attachment.get().getFileName());
    }

    @Test
    @DisplayName("Verify successful downloading of attachment")
    public void testSuccessfulDownloadOfAttachment() throws Exception {
        uploadAttachment();

        MvcResult downloadResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/tasks/401/attachments/1")).andReturn();
        // verify status
        assertEquals(HttpStatus.OK.value(), downloadResult.getResponse().getStatus());

        // verify content
        assertArrayEquals(FILE_CONTENT, downloadResult.getResponse().getContentAsByteArray());
    }

    private MvcResult uploadAttachment() throws Exception {
        return mockMvc
                .perform(MockMvcRequestBuilders.multipart("/api/tasks/401/attachments").file(FILE))
                .andReturn();
    }
}
