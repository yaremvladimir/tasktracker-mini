package com.tasktracker.mini.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tasktracker.mini.TaskTrackerServerStarter;
import com.tasktracker.mini.dto.DivisionDto;
import com.tasktracker.mini.entity.Division;
import com.tasktracker.mini.repository.DivisionsRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TaskTrackerServerStarter.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class DivisionsControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DivisionsRepository divisionsRepository;

    private ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @Test
    @DisplayName("Verify successful adding and deleting of new division")
    public void testDivisionsInsertionAndDeletion() throws JsonProcessingException, Exception {
        String name = "Super division";
        DivisionDto divisionDto = DivisionDto.builder().name(name).build();
        MvcResult insertResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/divisions")
                .contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsBytes(divisionDto)))
                .andReturn();
        // verify that response is OK
        assertEquals(HttpStatus.OK.value(), insertResult.getResponse().getStatus());

        Optional<Division> division = divisionsRepository.findById(1L);
        // verify that division is in DB
        assertTrue(division.isPresent());

        // verify correctness of division name
        assertEquals(name, division.get().getName());


        // delete division
        MvcResult deleteResult = mockMvc.perform(MockMvcRequestBuilders.delete("/api/divisions/1")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

        // verify that response is OK
        assertEquals(HttpStatus.OK.value(), deleteResult.getResponse().getStatus());

        Optional<Division> noDivision = divisionsRepository.findById(1L);
        // verify that division is not in DB
        assertTrue(noDivision.isEmpty());
    }

    @Test
    @DisplayName("Verify throwing of DivisionIdValidator when requesting non-existent division id")
    public void testGettingDivisionWithWrongId() throws JsonProcessingException, Exception {
        String wrongId = "100500";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/divisions/" + wrongId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

        // verify correct status code
        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());

        // verify it contains error message
        assertTrue(new String(result.getResponse().getContentAsByteArray())
                .contains("Division with id '" + wrongId + "' not found or missing"));
    }
}
