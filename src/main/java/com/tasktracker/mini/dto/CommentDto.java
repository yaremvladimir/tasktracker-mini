package com.tasktracker.mini.dto;

import javax.validation.constraints.NotEmpty;

import com.tasktracker.mini.constraint.validation.UserIdValidation;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommentDto {
    private Long id;

    @NotEmpty(message = "Comment cannot be empty")
    private String text;

    private Long taskId;

    @UserIdValidation
    private Long authorId;
}
