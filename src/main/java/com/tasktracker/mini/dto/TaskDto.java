package com.tasktracker.mini.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import com.tasktracker.mini.constraint.validation.StatusIdValidation;
import com.tasktracker.mini.constraint.validation.UserIdValidation;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TaskDto {
    private Long id;

    @NotEmpty(message = "Title cannot be empty")
    private String title;

    @NotEmpty(message = "Description cannot be empty")
    private String description;

    @UserIdValidation
    private Long assigneeId;
    
    @UserIdValidation
    private Long reporterId;
    
    @StatusIdValidation
    private Long statusId;

    private Date createdAt;
}
