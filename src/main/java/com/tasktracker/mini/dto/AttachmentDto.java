package com.tasktracker.mini.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AttachmentDto {
    private Long id;
    private String fileName;
    private Long taskId;
}
