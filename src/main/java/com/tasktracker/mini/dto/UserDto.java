package com.tasktracker.mini.dto;

import javax.validation.constraints.NotEmpty;

import com.tasktracker.mini.constraint.validation.DivisionIdValidation;

import lombok.Data;

@Data
public class UserDto {
    private Long id;

    @NotEmpty(message = "User name cannot be empty")
    private String name;

    @DivisionIdValidation
    private Long divisionId;
}
