package com.tasktracker.mini.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.tasktracker.mini.constraint.validation.AttachmentIdValidation;
import com.tasktracker.mini.constraint.validation.TaskIdValidation;
import com.tasktracker.mini.dto.AttachmentDto;
import com.tasktracker.mini.entity.Attachment;
import com.tasktracker.mini.service.AttachmentTaskService;

@Component
public class AttachmentsTasksSubController {
    private AttachmentTaskService attachmentsTaskService;

    @Autowired
    public AttachmentsTasksSubController(AttachmentTaskService attachmentsTaskService) {
        this.attachmentsTaskService = attachmentsTaskService;
    }

    @GetMapping("/{taskId}/attachments")
    public Page<AttachmentDto> getAttachments(@PathVariable @TaskIdValidation Long taskId,
                                              Pageable pageable) {
        return attachmentsTaskService.getAttachments(taskId, pageable).map(this::toDto);
    }

    @PostMapping("/{taskId}/attachments")
    public void uploadAttachment(@PathVariable @TaskIdValidation Long taskId,
                                 @RequestParam("file") MultipartFile file) {
        attachmentsTaskService.uploadAttachment(file, taskId);
    }

    @GetMapping("/{taskId}/attachments/{attachmentId}")
    public ResponseEntity<Resource> downloadAttachment(@PathVariable @TaskIdValidation Long taskId,
                                                       @PathVariable @AttachmentIdValidation Long attachmentId,
                                                       HttpServletRequest request) {
        Resource resource = attachmentsTaskService.downloadAttachment(attachmentId, taskId);
        String contentType = null;
        try {
            contentType =
                    request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        }

        contentType = Optional.ofNullable(contentType).orElse("application/octet-stream");

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private AttachmentDto toDto(Attachment attachment) {
        return AttachmentDto.builder().id(attachment.getId()).fileName(attachment.getFileName())
                .taskId(attachment.getTask().getId()).build();
    }
}
