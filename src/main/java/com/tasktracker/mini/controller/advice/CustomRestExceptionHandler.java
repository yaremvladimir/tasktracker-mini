package com.tasktracker.mini.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.common.collect.Iterables;
import com.tasktracker.mini.exception.FileNotFoundInStorageException;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                               HttpHeaders headers,
                                                               HttpStatus status,
                                                               WebRequest request) {
        if (ex.getCause() instanceof InvalidFormatException) {
            return handleInvalidFormatException((InvalidFormatException) ex.getCause(), headers,
                    status, request);
        } else {
            return super.handleHttpMessageNotReadable(ex, headers, status, request);
        }
    }

    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex,
                                                          Object body,
                                                          HttpHeaders headers,
                                                          HttpStatus status,
                                                          WebRequest request) {
        Object filledBody =
                body == null
                        ? new ApiError(status, ex.getLocalizedMessage(),
                                "Error occurred: " + ex.getLocalizedMessage())
                        : body;
        return super.handleExceptionInternal(ex, filledBody, headers, status, request);
    }

    private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ife,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
        String message = "'" + ife.getValue() + "' cannot be deserialized to "
                + ife.getTargetType().getSimpleName();
        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, ife.getLocalizedMessage(), message);
        return handleExceptionInternal(ife, apiError, headers, status, request);
    }


    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex,
                                                                        WebRequest request) {
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        List<String> errors = new ArrayList<String>(constraintViolations.size());
        errors.addAll(constraintViolations.stream()
                .map(cv -> Iterables.getLast(cv.getPropertyPath()) + ": " + cv.getMessage())
                .collect(Collectors.toList()));
        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, "Constraint validation failed", errors);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(),
                request);
    }

    @ExceptionHandler(value = {FileNotFoundInStorageException.class})
    protected ResponseEntity<Object> handleFileNotFoundInStorageException(FileNotFoundInStorageException ex,
                                                                          WebRequest request) {
        ApiError apiError =
                new ApiError(HttpStatus.NOT_FOUND, "File not found", ex.getLocalizedMessage());
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(),
                request);
    }


    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex,
                                                            WebRequest request) {
        ex.printStackTrace();
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Some error occured",
                ex.getLocalizedMessage());
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(),
                request);
    }

}
