package com.tasktracker.mini.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tasktracker.mini.constraint.validation.DivisionIdValidation;
import com.tasktracker.mini.dto.DivisionDto;
import com.tasktracker.mini.entity.Division;
import com.tasktracker.mini.repository.DivisionsRepository;

@Validated
@RestController
@RequestMapping("/api/divisions")
public class DivisionsController {
    private DivisionsRepository divisionRepository;

    @Autowired
    public DivisionsController(DivisionsRepository divisionRepository) {
        this.divisionRepository = divisionRepository;
    }

    @GetMapping
    public Page<Division> getDivisions(Pageable pageable) {
        return divisionRepository.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Optional<Division> findById(@PathVariable("id") @DivisionIdValidation Long id) {
        return divisionRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @DivisionIdValidation Long id) {
        divisionRepository.deleteById(id);
    }

    @PostMapping
    public void addUser(@Valid @RequestBody DivisionDto division) {
        divisionRepository.insert(toEntity(division));
    }

    private Division toEntity(DivisionDto dto) {
        return Division.builder()
                .name(dto.getName())
                .build();
    }
}
