package com.tasktracker.mini.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tasktracker.mini.constraint.validation.UserIdValidation;
import com.tasktracker.mini.dto.UserDto;
import com.tasktracker.mini.entity.Division;
import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.service.DefaultUserService;

@Validated
@RestController
@RequestMapping("/api/users")
public class UsersController {
    private DefaultUserService userService;

    @Autowired
    public UsersController(DefaultUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Page<User> getUsers(Pageable pageable) {
        return userService.getUsers(pageable);
    }

    @GetMapping("/{id}")
    public Optional<User> findById(@PathVariable("id") @UserIdValidation Long id) {
        return userService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @UserIdValidation Long id) {
        userService.delete(id);
    }

    @PutMapping("/{id}")
    public void delete(@PathVariable("id") @UserIdValidation Long id,
                       @Valid @RequestBody UserDto user) {
        user.setId(id);
        userService.update(toEntity(user));
    }

    @PostMapping
    public void createUser(@Valid @RequestBody UserDto user) {
        user.setId(null);
        userService.create(toEntity(user));
    }

    private User toEntity(UserDto dto) {
        return User.builder().name(dto.getName())
                .division(Division.builder().id(dto.getDivisionId()).build()).build();
    }
}
