package com.tasktracker.mini.controller;

import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tasktracker.mini.constraint.validation.AttachmentIdValidation;
import com.tasktracker.mini.constraint.validation.CommentIdValidation;
import com.tasktracker.mini.constraint.validation.TaskIdValidation;
import com.tasktracker.mini.dto.AttachmentDto;
import com.tasktracker.mini.dto.CommentDto;
import com.tasktracker.mini.dto.TaskDto;
import com.tasktracker.mini.entity.Status;
import com.tasktracker.mini.entity.Task;
import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.service.DefaultTaskService;

@Validated
@RestController
@RequestMapping("/api/tasks")
public class TasksController {
    private DefaultTaskService taskService;
    private CommentsTasksSubController commentsSubController;
    private AttachmentsTasksSubController attachmentsSubController;

    @Autowired
    public TasksController(DefaultTaskService taskService,
            CommentsTasksSubController commentsSubController,
            AttachmentsTasksSubController attachmentsSubController) {
        this.taskService = taskService;
        this.commentsSubController = commentsSubController;
        this.attachmentsSubController = attachmentsSubController;
    }

    @GetMapping
    public Page<Task> getTasks(@RequestParam("asigneeDivisionId") Optional<Long> asigneeDivisionId,
                               Pageable pageable) {
        return taskService.getTasks(asigneeDivisionId, pageable);
    }

    @GetMapping("/{id}")
    public Optional<Task> findById(@PathVariable @TaskIdValidation Long id) {
        return taskService.findById(id);
    }

    @PutMapping("/{id}")
    public void updateTask(@PathVariable @TaskIdValidation Long id,
                           @Valid @RequestBody TaskDto task) {
        task.setId(id);
        taskService.update(toEntity(task));
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable @TaskIdValidation Long id) {
        taskService.delete(id);
    }

    @PostMapping
    public void createTask(@Valid @RequestBody TaskDto task) {
        task.setId(null);
        taskService.create(toEntity(task));
    }


    @GetMapping("/{taskId}/comments")
    public Page<CommentDto> findComments(@PathVariable @TaskIdValidation Long taskId,
                                         Pageable pageable) {
        return commentsSubController.findComments(taskId, pageable);
    }

    @PostMapping("/{taskId}/comments")
    public void addComment(@PathVariable @TaskIdValidation Long taskId,
                           @Valid @RequestBody CommentDto comment) {
        commentsSubController.addComment(taskId, comment);
    }

    @PutMapping("/{taskId}/comments/{commentId}")
    public void updateComment(@PathVariable @TaskIdValidation Long taskId,
                              @PathVariable @CommentIdValidation Long commentId,
                              @RequestBody CommentDto comment) {
        commentsSubController.updateComment(taskId, commentId, comment);
    }

    @DeleteMapping("/{taskId}/comments/{commentId}")
    public void deleteComment(@PathVariable @TaskIdValidation Long taskId,
                              @PathVariable @CommentIdValidation Long commentId) {
        commentsSubController.deleteComment(taskId, commentId);
    }


    @GetMapping("/{taskId}/attachments")
    public Page<AttachmentDto> getAttachments(@PathVariable @TaskIdValidation Long taskId,
                                              Pageable pageable) {
        return attachmentsSubController.getAttachments(taskId, pageable);
    }

    @PostMapping("/{taskId}/attachments")
    public void uploadAttachment(@PathVariable @TaskIdValidation Long taskId,
                                 @RequestParam("file") MultipartFile file) {
        attachmentsSubController.uploadAttachment(taskId, file);
    }

    @GetMapping("/{taskId}/attachments/{attachmentId}")
    public ResponseEntity<Resource> downloadAttachment(@PathVariable @TaskIdValidation Long taskId,
                                                       @PathVariable @AttachmentIdValidation Long attachmentId,
                                                       HttpServletRequest request) {
        return attachmentsSubController.downloadAttachment(taskId, attachmentId, request);
    }

    private Task toEntity(TaskDto taskDto) {
        return Task.builder().id(taskDto.getId()).title(taskDto.getTitle())
                .description(taskDto.getDescription())
                .assignee(User.builder().id(taskDto.getAssigneeId()).build())
                .reporter(User.builder().id(taskDto.getReporterId()).build())
                .status(Status.builder().id(taskDto.getStatusId()).build()).createdAt(new Date())
                .build();
    }

}
