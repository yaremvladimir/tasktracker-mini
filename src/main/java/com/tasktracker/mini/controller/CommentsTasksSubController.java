package com.tasktracker.mini.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.dto.CommentDto;
import com.tasktracker.mini.entity.Comment;
import com.tasktracker.mini.entity.Task;
import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.service.CommentTaskService;

@Component
public class CommentsTasksSubController {
    private CommentTaskService commentsTaskService;

    @Autowired
    public CommentsTasksSubController(CommentTaskService commentsTaskService) {
        this.commentsTaskService = commentsTaskService;
    }

    public Page<CommentDto> findComments(Long taskId,
                                         Pageable pageable) {
        return commentsTaskService.getComments(taskId, pageable).map(this::toDto);
    }

    public void addComment(Long taskId,
                           CommentDto comment) {
        comment.setId(null);
        comment.setTaskId(taskId);
        commentsTaskService.addComment(toEntity(comment));
    }

    public void updateComment(Long taskId,
                              Long commentId,
                              CommentDto comment) {
        comment.setId(commentId);
        comment.setTaskId(taskId);
        commentsTaskService.updateComment(toEntity(comment));
    }

    public void deleteComment(Long taskId,
                              Long commentId) {
        commentsTaskService.deleteComment(commentId, taskId);
    }

    private CommentDto toDto(Comment comment) {
        return CommentDto.builder().id(comment.getId()).authorId(comment.getAuthor().getId())
                .taskId(comment.getTask().getId()).text(comment.getText()).build();

    }

    private Comment toEntity(CommentDto comment) {
        return Comment.builder().id(comment.getId()).text(comment.getText())
                .task(Task.builder().id(comment.getTaskId()).build())
                .author(User.builder().id(comment.getAuthorId()).build()).build();

    }
}
