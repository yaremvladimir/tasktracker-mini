package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.UserIdValidation;
import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.repository.UsersRepository;

@Component
public class UserIdValidator extends AbstractRepositoryIdValidator<UserIdValidation, Long, User, UsersRepository> {
    @Autowired
    private UsersRepository userRepository;

    @Override
    public UsersRepository getRepository() {
        return userRepository;
    }

}
