package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.CommentIdValidation;
import com.tasktracker.mini.entity.Comment;
import com.tasktracker.mini.repository.CommentsRepository;

@Component
public class CommentIdValidator extends AbstractRepositoryIdValidator<CommentIdValidation, Long, Comment, CommentsRepository> {
    @Autowired
    private CommentsRepository commentsRepository;

    @Override
    public CommentsRepository getRepository() {
        return commentsRepository;
    }

}
