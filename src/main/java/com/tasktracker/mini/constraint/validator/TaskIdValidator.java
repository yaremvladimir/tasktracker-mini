package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.TaskIdValidation;
import com.tasktracker.mini.entity.Task;
import com.tasktracker.mini.repository.TasksRepository;

@Component
public class TaskIdValidator extends AbstractRepositoryIdValidator<TaskIdValidation, Long, Task, TasksRepository> {
    @Autowired
    private TasksRepository tasksRepository;

    @Override
    public TasksRepository getRepository() {
        return tasksRepository;
    }

}
