package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.AttachmentIdValidation;
import com.tasktracker.mini.entity.Attachment;
import com.tasktracker.mini.repository.AttachmentsRepository;

@Component
public class AttachmentIdValidator extends AbstractRepositoryIdValidator<AttachmentIdValidation, Long, Attachment, AttachmentsRepository> {
    @Autowired
    private AttachmentsRepository attachmentsRepository;

    @Override
    public AttachmentsRepository getRepository() {
        return attachmentsRepository;
    }


}
