package com.tasktracker.mini.constraint.validator;

import java.lang.annotation.Annotation;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.tasktracker.mini.repository.AbstractRepository;

public abstract class AbstractRepositoryIdValidator<A extends Annotation, ID, T, R extends AbstractRepository<T, ID>> implements ConstraintValidator<A, ID>  {
    public abstract R getRepository();

    @Override
    public boolean isValid(ID value, ConstraintValidatorContext context) {
        return Optional.ofNullable(value).isPresent() && getRepository().findById(value).isPresent();
    }
}
