package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.StatusIdValidation;
import com.tasktracker.mini.entity.Status;
import com.tasktracker.mini.repository.StatusRepository;

@Component
public class StatusIdValidator extends AbstractRepositoryIdValidator<StatusIdValidation, Long, Status, StatusRepository> {
    @Autowired
    private StatusRepository statusRepository;

    @Override
    public StatusRepository getRepository() {
        return statusRepository;
    }

}
