package com.tasktracker.mini.constraint.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.constraint.validation.DivisionIdValidation;
import com.tasktracker.mini.entity.Division;
import com.tasktracker.mini.repository.DivisionsRepository;

@Component
public class DivisionIdValidator extends AbstractRepositoryIdValidator<DivisionIdValidation, Long, Division, DivisionsRepository> {
    @Autowired
    private DivisionsRepository divisionRepository;

    @Override
    public DivisionsRepository getRepository() {
        return divisionRepository;
    }
}
