package com.tasktracker.mini.service.external;

public interface RatingService {
    public Integer getRatingByUserId(Long userId);
}
