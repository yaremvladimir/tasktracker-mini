package com.tasktracker.mini.service.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ExternalRatingService implements RatingService {

    private String ratingServiceUrl;

    @Autowired
    public ExternalRatingService(@Value("{service.rating.url}") String ratingServiceUrl) {
        this.ratingServiceUrl = ratingServiceUrl;
    }

    @Override
    @HystrixCommand(fallbackMethod = "defaultRating")
    public Integer getRatingByUserId(Long userId) {
        return new RestTemplate().getForObject(ratingServiceUrl + "/rating/{userId}", Integer.class,
                userId);
    }

    @SuppressWarnings("unused") // used by Hystrix above
    private Integer defaultRating(Long userId) {
        return 0;
    }

}
