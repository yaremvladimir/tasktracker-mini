package com.tasktracker.mini.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.repository.UsersRepository;
import com.tasktracker.mini.wrapper.UserRatingWrapper;

@Service
public class DefaultUserService implements UserService {
    private UsersRepository usersRepository;
    private UserRatingWrapper userRatingService;

    @Autowired
    public DefaultUserService(UsersRepository usersRepository,
            UserRatingWrapper userRatingService) {
        this.usersRepository = usersRepository;
        this.userRatingService = userRatingService;
    }

    @Override
    public Page<User> getUsers(Pageable pageable) {
        return usersRepository.findAll(pageable).map(userRatingService::wrap);
    }

    @Override
    public Optional<User> findById(Long id) {
        return usersRepository.findById(id).map(userRatingService::wrap);
    }

    @Override
    public void delete(Long id) {
        usersRepository.deleteById(id);
    }

    @Override
    public void create(User user) {
        usersRepository.insert(user);
    }

    @Override
    public void update(User user) {
        usersRepository.update(user);
    }

}
