package com.tasktracker.mini.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tasktracker.mini.entity.Comment;

public interface CommentTaskService {
    /**
     * Get comments for task
     * 
     * @param taskId
     * @param pageable - object for manipulating pages and sorting
     * @return page of comments
     */
    public Page<Comment> getComments(Long taskId, Pageable pageable);


    /**
     * Add comment to task
     * 
     * @param comment
     */
    public void addComment(Comment comment);

    /**
     * Update comment
     * 
     * @param comment
     */
    public void updateComment(Comment comment);

    /**
     * Delete comment for task
     * 
     * @param commentId
     * @param taskId
     */
    public void deleteComment(Long commentId, Long taskId);
}
