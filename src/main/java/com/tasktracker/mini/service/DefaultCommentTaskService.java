package com.tasktracker.mini.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tasktracker.mini.entity.Comment;
import com.tasktracker.mini.repository.CommentsRepository;

@Service
public class DefaultCommentTaskService implements CommentTaskService {
    private CommentsRepository commentsRepository;

    @Autowired
    public DefaultCommentTaskService(CommentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }

    @Override
    public Page<Comment> getComments(Long taskId, Pageable pageable) {
        return commentsRepository.findByTaskId(taskId, pageable);
    }

    @Override
    public void addComment(Comment comment) {
        commentsRepository.insert(comment);
    }

    @Override
    public void updateComment(Comment comment) {
        commentsRepository.update(comment);
    }

    @Override
    public void deleteComment(Long commentId, Long taskId) {
        commentsRepository.deleteByIdAndTaskId(commentId, taskId);
    }
}
