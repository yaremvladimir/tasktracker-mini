package com.tasktracker.mini.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tasktracker.mini.entity.Attachment;
import com.tasktracker.mini.entity.Task;
import com.tasktracker.mini.exception.FileNotFoundInStorageException;
import com.tasktracker.mini.repository.AttachmentsRepository;

@Service
public class DefaultAttachmentTaskService implements AttachmentTaskService {
    private FileStorageService fileStorageService;
    private AttachmentsRepository attachmentsRepository;

    @Autowired
    public DefaultAttachmentTaskService(FileStorageService fileStorageService,
                                         AttachmentsRepository attachmentsRepository) {
        this.fileStorageService = fileStorageService;
        this.attachmentsRepository = attachmentsRepository;
    }

    @Override
    public Page<Attachment> getAttachments(Long taskId, Pageable pageable) {
        return attachmentsRepository.findByTaskId(taskId, pageable);
    }

    @Override
    public void uploadAttachment(MultipartFile file, Long taskId) {
        String fileName = fileStorageService.storeFile(file);
        Attachment attachment = Attachment.builder().fileName(fileName)
                .task(Task.builder().id(taskId).build()).build();
        attachmentsRepository.insert(attachment);
    }

    @Override
    public Resource downloadAttachment(Long attachmentId, Long taskId) {
        Optional<Attachment> attachment =
                attachmentsRepository.findByIdAndTaskId(attachmentId, taskId);
        if (attachment.isEmpty()) {
            throw new FileNotFoundInStorageException("unknown");
        }
        String fileName = attachment.get().getFileName();
        return fileStorageService.loadFileAsResource(fileName);
    }

}
