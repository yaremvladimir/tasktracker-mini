package com.tasktracker.mini.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tasktracker.mini.entity.Task;
import com.tasktracker.mini.repository.TasksRepository;
import com.tasktracker.mini.wrapper.UserRatingWrapper;

@Service
public class DefaultTaskService implements TaskService {
    private TasksRepository tasksRepository;
    private UserRatingWrapper userRatingService;

    @Autowired
    public DefaultTaskService(TasksRepository tasksRepository,
                              UserRatingWrapper userRatingService) {
        this.tasksRepository = tasksRepository;
        this.userRatingService = userRatingService;
    }

    @Override
    public Page<Task> getTasks(Optional<Long> asigneeDivisionId, Pageable pageable) {
        return pageOfTasks(asigneeDivisionId, pageable).map(this::wrapUsersWithRating);
    }

    @Override
    public Optional<Task> findById(Long id) {
        return tasksRepository.findById(id).map(this::wrapUsersWithRating);
    }

    @Override
    public void create(Task task) {
        tasksRepository.insert(task);
    }

    @Override
    public void update(Task task) {
        tasksRepository.update(task);
    }

    @Override
    public void delete(Long taskId) {
        tasksRepository.deleteById(taskId);
    }

    private Page<Task> pageOfTasks(Optional<Long> asigneeDivisionId, Pageable pageable) {
        return asigneeDivisionId.map(d -> tasksRepository.findByDivision(d, pageable))
                .orElse(tasksRepository.findAll(pageable));
    }

    private Task wrapUsersWithRating(Task t) {
        t.setAssignee(userRatingService.wrap(t.getAssignee()));
        t.setReporter(userRatingService.wrap(t.getReporter()));
        return t;
    }
}
