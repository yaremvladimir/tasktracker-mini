package com.tasktracker.mini.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tasktracker.mini.entity.User;

public interface UserService {
    /**
     * Get users
     * 
     * @param pageable - object for manipulating pages and sorting
     * @return page of users
     */
    public Page<User> getUsers(Pageable pageable);

    /**
     * Get user by id
     * 
     * @param id - user id
     * @return user if exists
     */
    public Optional<User> findById(Long id);

    /**
     * Add new user
     * 
     * @param user
     */
    public void create(User user);

    /**
     * Update user
     * 
     * @param user
     */
    public void update(User user);

    /**
     * Delete user
     * 
     * @param userId
     */
    public void delete(Long userId);
}
