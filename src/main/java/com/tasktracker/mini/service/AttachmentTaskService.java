package com.tasktracker.mini.service;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.tasktracker.mini.entity.Attachment;

public interface AttachmentTaskService {
    /**
     * Get attachments for task
     * 
     * @param taskId
     * @param pageable - object for manipulating pages and sorting
     * @return page of attachment details
     */
    public Page<Attachment> getAttachments(Long taskId, Pageable pageable);

    /**
     * Upload attachment file
     * 
     * @param file containing file metadata and link to InputStream
     * @param taskId
     */
    public void uploadAttachment(MultipartFile file, Long taskId);

    /**
     * Download attachment file
     * 
     * @param attachmentId
     * @param taskId
     * @return resource containing file
     */
    public Resource downloadAttachment(Long attachmentId, Long taskId);
}
