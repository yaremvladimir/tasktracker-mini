package com.tasktracker.mini.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tasktracker.mini.entity.Task;

public interface TaskService {
    /**
     * Get tasks
     * 
     * @param asigneeDivisionId - filter by assignee division id
     * @param pageable - object for manipulating pages and sorting
     * @return page of tasks
     */
    public Page<Task> getTasks(Optional<Long> asigneeDivisionId, Pageable pageable);

    /**
     * Find task by id
     * 
     * @param id - task id
     * @return task if exists
     */
    public Optional<Task> findById(Long id);

    /**
     * Create new task
     * 
     * @param task
     */
    public void create(Task task);

    /**
     * Update existing task
     * 
     * @param task
     */
    public void update(Task task);

    /**
     * Delete task
     * 
     * @param taskId
     */
    public void delete(Long taskId);
}
