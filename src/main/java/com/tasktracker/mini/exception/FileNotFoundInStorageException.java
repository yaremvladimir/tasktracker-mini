package com.tasktracker.mini.exception;

public class FileNotFoundInStorageException extends RuntimeException {
    private static final long serialVersionUID = 1981732726751741298L;

    public FileNotFoundInStorageException(String filePath) {
        super("File with name '" + filePath + "' is not found.");
    }
}
