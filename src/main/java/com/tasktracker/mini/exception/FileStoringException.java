package com.tasktracker.mini.exception;

public class FileStoringException extends RuntimeException {
    private static final long serialVersionUID = 4302784777348626654L;

    public static FileStoringException cannotStoreFile(String filePath, Throwable e) {
        return new FileStoringException("Cannot store file " + filePath, e);
    }

    public static FileStoringException cannotCreateDirectory(String directoryName, Throwable e) {
        return new FileStoringException("Cannot create directory " + directoryName, e);
    }

    private FileStoringException(String text, Throwable e) {
        super(text, e);
    }
}
