package com.tasktracker.mini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class TaskTrackerServerStarter {
    public static void main(String[] args) {
        SpringApplication.run(TaskTrackerServerStarter.class, args);
    }
}
