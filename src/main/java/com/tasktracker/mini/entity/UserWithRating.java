package com.tasktracker.mini.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserWithRating extends User {
    private Integer rating;

    public UserWithRating(User user, Integer rating) {
        super(user.getId(), user.getName(), user.getDivision());
        this.rating = rating;
    }
}
