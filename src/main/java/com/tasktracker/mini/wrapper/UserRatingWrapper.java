package com.tasktracker.mini.wrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasktracker.mini.entity.User;
import com.tasktracker.mini.entity.UserWithRating;
import com.tasktracker.mini.service.external.RatingService;

@Component
public class UserRatingWrapper {
    private RatingService ratingService;

    @Autowired
    public UserRatingWrapper(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    public UserWithRating wrap(User user) {
        Integer rating = ratingService.getRatingByUserId(user.getId());
        return new UserWithRating(user, rating);
    }
}
