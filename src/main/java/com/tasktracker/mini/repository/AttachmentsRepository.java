package com.tasktracker.mini.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.Attachment;

@Repository
public interface AttachmentsRepository extends AbstractRepository<Attachment, Long> {

    public Page<Attachment> findByTaskId(Long taskId, Pageable pageable);

    public Optional<Attachment> findByIdAndTaskId(Long id, Long taskId);
}
