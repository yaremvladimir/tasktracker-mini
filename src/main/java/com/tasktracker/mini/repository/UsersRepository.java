package com.tasktracker.mini.repository;

import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.User;

@Repository
public interface UsersRepository extends AbstractRepository<User, Long> {
}
