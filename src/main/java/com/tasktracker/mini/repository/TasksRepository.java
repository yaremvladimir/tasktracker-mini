package com.tasktracker.mini.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.Task;

@Repository
public interface TasksRepository extends AbstractRepository<Task, Long> {
    @Query("from Task t where t.assignee.division.id=:divisionId")
    public Page<Task> findByDivision(Long divisionId, Pageable pageable);
}
