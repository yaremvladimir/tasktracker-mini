package com.tasktracker.mini.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AbstractRepository<T, ID> extends CrudRepository<T, ID> {
    public Page<T> findAll(Pageable pageable);

    public Optional<T> findById(ID id);

    public default void insert(T comment) {
        save(comment);
    }

    public default void update(T comment) {
        save(comment);
    }

    public void deleteById(ID id);
}
