package com.tasktracker.mini.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.Comment;

@Repository
public interface CommentsRepository extends AbstractRepository<Comment, Long> {
    public Page<Comment> findByTaskId(Long taskId, Pageable pageable);

    public void deleteByIdAndTaskId(Long id, Long taskId);
}
