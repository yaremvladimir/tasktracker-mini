package com.tasktracker.mini.repository;

import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.Status;

@Repository
public interface StatusRepository extends AbstractRepository<Status, Long> {
}
