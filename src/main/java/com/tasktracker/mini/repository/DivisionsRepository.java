package com.tasktracker.mini.repository;

import org.springframework.stereotype.Repository;

import com.tasktracker.mini.entity.Division;

@Repository
public interface DivisionsRepository extends AbstractRepository<Division, Long> {
}
