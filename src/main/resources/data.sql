insert into division(id, name) values(101, 'Engineering');
insert into division(id, name) values(102, 'Management');
insert into division(id, name) values(103, 'Support');

insert into user(id, name, division_id) values(201, 'Marcus Oldman', 101);
insert into user(id, name, division_id) values(202, 'Howard Volovitz', 101);
insert into user(id, name, division_id) values(203, 'Tyler Willmington', 101);
insert into user(id, name, division_id) values(204, 'Ray Randolf', 102);
insert into user(id, name, division_id) values(205, 'Tim Jones', 102);
insert into user(id, name, division_id) values(206, 'Bill Williams', 103);

insert into status(id, name) values(301, 'Opened');
insert into status(id, name) values(302, 'In Progress');
insert into status(id, name) values(303, 'In Code Review');
insert into status(id, name) values(304, 'In QA');
insert into status(id, name) values(305, 'PO Acceptance');
insert into status(id, name) values(306, 'Done');

insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(401, '2021-01-24 22:27:44Z', 'Task 1', 'This is task for making technical design for feature 1', 301, 201, 202);
insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(402, '2021-01-24 22:56:22Z', 'Task 2', 'This is task for implementation of feature 1', 302, 203, 202);
insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(403, '2021-01-24 22:58:35Z', 'Task 3', 'Here need to be performed load testing of Bonus Service', 303, 204, 206);
insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(404, '2021-01-24 23:01:48Z', 'Task 4', 'Implement making coffee using 2 spoons and tea', 304, 205, 203);
insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(405, '2021-01-24 23:02:53Z', 'Task 5', 'Make raw implementation of Indexing service', 305, 204, 201);
insert into task(id, created_at, title, description, status_id, assignee_user_id, reporter_user_id) 
	values(406, '2021-01-24 23:03:03Z', 'Task 6', 'Customer OxyFone complains on bonuses not topped up after payment via MonoBank. Need to investigate.', 306, 201, 206);

insert into comment(id, author_id, task_id, text) values(501, 201, 401, 'This task should be discussed at nearest hardening');
insert into comment(id, author_id, task_id, text) values(502, 202, 401, 'Discussed for sure, +1');
insert into comment(id, author_id, task_id, text) values(503, 203, 401, 'Discussed for sure, agree, +2');
insert into comment(id, author_id, task_id, text) values(504, 205, 404, 'What about matcha latte?');
insert into comment(id, author_id, task_id, text) values(505, 206, 406, 'Customer said he fixted it itself. Can be closed.');