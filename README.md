# Task Manager mini
Minimal implementation of task manager

### Features
- Creating tasks for users
- Adding comments to tasks
- Adding attachments for better understanding of tasks

### How to start
- Install Java, it not yet installed
- Download jar from `Downloads` section in BitBucket sidebar
- Execute `java -jar tasktracker-server.jar`
- Profit



### REST API (Several, as an example)
For more, explore [http://localhost:9093/swagger-ui/](http://localhost:9093/swagger-ui/ "http://localhost:9093/swagger-ui/")

##### Get tasks
Getting tasks with possibility to filter out tasks by assignee division id
###### Format
| Method  | URI  |
| ------------ | ------------ |
| POST  | /api/tasks  |

###### Query params
| Name  | Explanation  | Default value  |
| ------------ | ------------ | ------------ |
| asigneeDivisionId  | Assignee division id  |  - |
| page  |  Page id. Starts from 0. | 0  |
| size  |  Page size | 20  |
| sort  |  Sorting criteria. Has 2 params divided by coma. First param - field to sort on, second param - sorting order. E.g. `sort=title,desc` | id,asc  |
Example: `POST` `[API_HOST]/api/tasks?asigneeDivisionId=101&page=1&size=5&sort=title,desc`

##### Add task
Adding new task
###### Format
| Method  | URI  |
| ------------ | ------------ |
| POST  | /api/tasks  |

###### Query params
none

##### Request body sample
```json
{"title": "Task title3", "description": "Task description", "assigneeId":201, "reporterId": 202, "statusId": 301}
```

##### Update task
Updating existing task
###### Format
| Method  | URI  |
| ------------ | ------------ |
| PUT  | /api/tasks/{taskId}  |

###### Path params
| Name  | Explanation  |
| ------------ | ------------ |
| taskId  | Task id  |

###### Query params
none

##### Request body sample
```json
{"title": "Task title3", "description": "Task description changed, assignee too", "assigneeId":203, "reporterId": 202, "statusId": 304}
```

##### Get task comments
Getting task comments
###### Format
| Method  | URI  |
| ------------ | ------------ |
| GET  | /api/task/{taskId}/comments  |

###### Path params
| Name  | Explanation  |
| ------------ | ------------ |
| taskId  | Task id  |

###### Query params
| Name  | Explanation  | Default value  |
| ------------ | ------------ | ------------ |
| page  |  page=1 | 0  |
| size  |  size=1 | 20  |
| sort  |  sort=title,desc | id,asc  |
Example: `GET` `[API_HOST]/api/tasks/401/comments?page=1&size=5&sort=title,desc`

##### Delete task comment
Deleting task comment
###### Format
| Method  | URI  |
| ------------ | ------------ |
| DELETE  | /api/task/{taskId}/comments/{commentId}  |

###### Path params
| Name  | Explanation  |
| ------------ | ------------ |
| taskId  | Task id  |
| commentId  | Comment id  |

###### Query params
none

Example: `DELETE` `[API_HOST]/api/tasks/401/comments/501`


##### Get task attachments
Getting task attachments
###### Format
| Method  | URI  |
| ------------ | ------------ |
| GET  | /api/task/{taskId}/attachments  |

###### Path params
| Name  | Explanation  |
| ------------ | ------------ |
| taskId  | Task id  |

###### Query params
| Name  | Explanation  | Default value  |
| ------------ | ------------ | ------------ |
| page  |  page=1 | 0  |
| size  |  size=1 | 20  |
| sort  |  sort=title,desc | id,asc  |
Example: `GET` `[API_HOST]/api/tasks/401/attachments?page=1&size=5&sort=title,desc`

##### Download task attachment
Getting task attachments
###### Format
| Method  | URI  |
| ------------ | ------------ |
| GET  | /api/task/{taskId}/attachments/{attachmentId}  |

###### Path params
| Name  | Explanation  |
| ------------ | ------------ |
| taskId  | Task id  |
| attachmentId  | Attachment id  |

###### Query params
none

Example: `GET` `[API_HOST]/api/tasks/401/attachments/601`

### Technical details
##### Technologies used
- Java 11+
- Maven 3.5+
- As DB - H2
